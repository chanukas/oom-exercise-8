package com.company.refactor;

import java.util.Collections;

import com.company.refactor.AmountAndRecordsPerBank;
import com.company.refactor.Client;
import com.company.refactor.ConfirmationLetter;
import com.company.refactor.GenericRecord;
import com.company.refactor.HashBatchRecordsBalance;
import com.company.refactor.Record;
import com.company.refactor.command.FileUploadCommand;
import com.company.refactor.domain.TempRecord;
import com.company.refactor.parser.FileExtension;
import com.company.refactor.service.impl.Constants;
import com.sun.xml.internal.ws.client.RequestContext;

public class ConfirmationLetterGenerator {

    private LetterSelector letterSelector;
    
    public OurOwnByteArrayOutputStream letter(RequestContext context, FileUploadCommand fileUploadCommand,
                                              Client client, HashBatchRecordsBalance hashBatchRecordsBalance, String branchName,
                                              Listlt;AmountAndRecordsPerBankgt; bankMap, Listlt;com.example.record.domain.FaultRecordgt; faultyRecords,
                                              FileExtension extension, Listlt;Recordgt; records, Listlt;TempRecordgt; faultyAccountNumberRecordList,
                                              Listlt;TempRecordgt; sansDuplicateFaultRecordsList) {

        ConfirmationLetter letter = createConfirmationLetter(fileUploadCommand, client, hashBatchRecordsBalance,
                branchName, bankMap, faultyRecords, extension, records, faultyAccountNumberRecordList,
                sansDuplicateFaultRecordsList);

        OurOwnByteArrayOutputStream arrayOutputStream = generateConfirmationLetterAsPDF(client, letter);

        context.getConversationScope().asMap().put("dsbByteArrayOutputStream", arrayOutputStream);

        return arrayOutputStream;
    }


    public OurOwnByteArrayOutputStream generateConfirmationLetterAsPDF(
            Client client, ConfirmationLetter letter) {
        return letterSelector.generateLetter(client.getCreditDebit(), letter);
    }


    public ConfirmationLetter createConfirmationLetter(
            FileUploadCommand fileUploadCommand, Client client,
            HashBatchRecordsBalance hashBatchRecordsBalance, String branchName,
            Listlt;AmountAndRecordsPerBankgt; bankMap,
            Listlt;com.example.record.domain.FaultRecordgt; faultyRecords,
            FileExtension extension, Listlt;Recordgt; records,
            Listlt;TempRecordgt; faultyAccountNumberRecordList,
            Listlt;TempRecordgt; sansDuplicateFaultRecordsList) {

        ConfirmationLetter letter = new ConfirmationLetter();

        letter.setCurrency(records.get(0).getCurrency());
        letter.setExtension(extension);

        letter.setHashTotalCredit(hashBatchRecordsBalance.getHashTotalCredit().toString());
        letter.setHashTotalDebit(hashBatchRecordsBalance.getHashTotalDebit().toString());

        letter.setTotalProcessedRecords(hashBatchRecordsBalance.getRecordsTotal().toString());

        letter.setTransferType(hashBatchRecordsBalance.getCollectionType());
        letter.setBanks(bankMap);

        letter.setCreditingErrors(faultyRecords);
        letter.setClient(client);
        letter.setBranchName(branchName);

        ConfirmationLetterTotalsCalculator calculator
                = new ConfirmationLetterTotalsCalculator();
        Maplt;String, BigDecimalgt; recordAmount
                = calculator.calculateRetrievedAmounts(
                Collections.lt;GenericRecordgt;unmodifiableList(records),
                Collections.lt;GenericRecordgt;unmodifiableList(faultyAccountNumberRecordList),
                Collections.lt;GenericRecordgt;unmodifiableList(sansDuplicateFaultRecordsList),
                client);

        letter.setRetrievedAmountEur(recordAmount.get(Constants.CURRENCY_EURO));
        letter.setRetrievedAmountFL(recordAmount.get(Constants.CURRENCY_FL));
        letter.setRetrievedAmountUsd(recordAmount.get(Constants.CURRENCY_FL));

        letter.setBatchTotalDebit(hashBatchRecordsBalance.calculateTotalOverBatches(client.getAmountDivider(),
                Constants.CREDIT).toString());
        letter.setBatchTotalCredit(hashBatchRecordsBalance.calculateTotalOverBatches(client.getAmountDivider(),
                Constants.DEBIT).toString());

        letter.setTransactionCost(getTransactionCost(fileUploadCommand, hashBatchRecordsBalance));
        letter.setTotalRetrievedRecords(fileUploadCommand.getTotalRecords());
        return letter;
    }

    public String getTransactionCost(FileUploadCommand fileUploadCommand,
                                     HashBatchRecordsBalance hashBatchRecordsBalance) {

        String transactionCost = "";

        if (fileUploadCommand.hasFee()) {
            transactionCost = hashBatchRecordsBalance.getTotalFee().toString();
        }

        return transactionCost;
    }

    public void setLetterSelector(LetterSelector letterSelector) {
        this.letterSelector = letterSelector;
    }

}
