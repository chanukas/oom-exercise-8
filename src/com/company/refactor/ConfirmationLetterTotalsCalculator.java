package com.company.refactor;


import java.math.BigDecimal;
import java.util.Arrays;

import com.company.refactor.Client;
import com.company.refactor.Currency;
import com.company.refactor.service.impl.Constants;

public class ConfirmationLetterTotalsCalculator {

    private Listlt;Stringgt; currencies = Arrays.asList(Constants.CURRENCY_EURO,
    Constants.CURRENCY_FL, Constants.CURRENCY_USD);

    private class CreditDebitHolder {

        private Maplt;String, Maplt;String, BigDecimalgt;gt; values = new HashMaplt;String, Maplt;String, BigDecimalgt;gt;();

        public CreditDebitHolder() {
            values.put(Constants.DEBIT, new HashMaplt;String, BigDecimalgt;());
            values.put(Constants.CREDIT, new HashMaplt;String, BigDecimalgt;());
        }

        public BigDecimal getValue(String sign, String currency) {
            BigDecimal value = values.get(sign).get(currency);
            if (value == null) {
                value = BigDecimal.ZERO;
            }
            return value;
        }

        public void setValue(String currency, String sign, BigDecimal value) {
            values.get(sign).put(currency, value);
        }
    }

    private class RecordFilterStrategy {
        boolean filter(GenericRecord record) {
            return true;
        }
    }

    private RecordFilterStrategy sansAmountsFilter = new RecordFilterStrategy();
    private RecordFilterStrategy faultyAmountsFilter = new RecordFilterStrategy();
    private RecordFilterStrategy recordAmountsFilter = new RecordFilterStrategy() {
        boolean filter(GenericRecord record) {
            return record.isCounterTransferRecord()  !record.hasFee();
        }
    };
    private RecordFilterStrategy balancedFilter = new RecordFilterStrategy() {
        boolean filter(GenericRecord record) {
            return !record.hasFee()  record.isDebitRecord();
        }
    };

    private CreditDebitHolder recordAmounts = new CreditDebitHolder();
    private CreditDebitHolder sansAmounts = new CreditDebitHolder();
    private CreditDebitHolder faultyAccountRecordAmounts = new CreditDebitHolder();
    private CreditDebitHolder balancedAmounts = new CreditDebitHolder();

    private Utils utils;
    private Client client;

    
    public Maplt;String, BigDecimalgt; calculateRetrievedAmounts(
            Listlt;GenericRecordgt; records,
            Listlt;GenericRecordgt; faultyAccountNumberRecordList,
            Listlt;GenericRecordgt; sansDuplicateFaultRecordsList,
            Client client) {

        this.client = client;

        if (client.isBalanced()) {
            calculateTotalsOverRecords(records, balancedAmounts, balancedFilter);
        } else {
            calculateTotalsOverRecords(records, recordAmounts, recordAmountsFilter);
            calculateTotalsOverRecords(sansDuplicateFaultRecordsList, sansAmounts, sansAmountsFilter);
            calculateTotalsOverRecords(faultyAccountNumberRecordList, faultyAccountRecordAmounts, faultyAmountsFilter);
        }
        return calculateOverallTotalsForAllCurrencies();
    }

    private void calculateTotalsOverRecords(Listlt;GenericRecordgt; records,
                                            CreditDebitHolder amountsHolder,
                                            RecordFilterStrategy filterCommand) {

        for (GenericRecord record : records) {
            if (filterCommand.filter(record)) {
                addAmountToSignedTotal(record, amountsHolder);
            }
        }
    }

    private void addAmountToSignedTotal(GenericRecord record,
                                        CreditDebitHolder amountsHolder) {

        setRecordSignToClientSignIfUnset(record);
        setRecordCurrencyCodeToClientIfUnset(record);

        String currency = Currency.getCurrencyByCode(record.getCurrencyNumericCode());
        amountsHolder.setValue(currency, record.getSign(),
                amountsHolder.getValue(currency, record.getSign()).add(record.getAmountAsBigDecimal()));
    }

    private Maplt;String, BigDecimalgt; calculateOverallTotalsForAllCurrencies() {
        HashMaplt;String, BigDecimalgt; recordAmount = new HashMaplt;String, BigDecimalgt;();
        for (String currency : currencies) {
            recordAmount.put(currency, calculateOverallTotal(currency));
        }
        return recordAmount;
    }

    private BigDecimal calculateOverallTotal(String currency) {
        return calculateOverAllTotalForSign(currency, Constants.CREDIT)
                .subtract(calculateOverAllTotalForSign(currency, Constants.DEBIT)).abs();
    }

    private BigDecimal calculateOverAllTotalForSign(String currency, String sign) {
        return balancedAmounts.getValue(currency, sign)
                .add(recordAmounts.getValue(currency, sign)
                        .add(sansAmounts.getValue(currency, sign))
                        .subtract(faultyAccountRecordAmounts.getValue(currency, sign)));
    }

    private void setRecordCurrencyCodeToClientIfUnset(GenericRecord sansDupRec) {
        Integer currencyCode = sansDupRec.getCurrencyNumericCode();
        if (currencyCode == null) {
            Currency currency = utils.getDefaultCurrencyForClient(client);
            sansDupRec.setCurrencyNumericCode(currency.getCode());
        }
    }

    private void setRecordSignToClientSignIfUnset(GenericRecord tempRecord) {
        if (tempRecord.getSign() == null) {
            String sign = client.getCreditDebit();
            tempRecord.setSign(sign);
        }
    }
}
